# JGTD  
## 简介  
NSCAI发布的《最终报告》让我对选用java进行开发产生怀疑，不清楚会不会禁用中国的JDK（包括OpenJDK），暂时先观望。如有必要会转Rust或者C++。初三和初二（lsk的功劳最大）的作品。  
这个项目是受   https://github.com/NothingLeftProject/NothingLeft-Backend 的启发。  
#### 本软件提供自行规划时间，在制定时间里不允许操作PC以及日历的功能。  
# 参与开发（约束）  
## 提交（相对规范的使用git）  
请向dev分支提交代码  
master分支将会设置为保护分支  
从dev分支合并到master请使用git merge --squash dev  
再git commit  

## 其他

使用OpenJDK15(Arch的最新版本)  

# 更新日志  
详细请看git信息  
# 使用方法  
1.运行jar包  
2.打开网页[localhost](http://localhost:8080/)  
3.使用  
#  TODO  
1. ~~vue前端~~   
2. springboot接口  
3. 完善前端  
4. 重写[TimeFileManager](src/main/java/com/JGTD/Backend/TimeFileManager.java)，使其使用XML为配置文件  