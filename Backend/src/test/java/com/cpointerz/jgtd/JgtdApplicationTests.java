package com.cpointerz.jgtd;

import com.cpointerz.jgtd.mapper.DataMapper;
import com.cpointerz.jgtd.model.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
@Slf4j
@SpringBootTest
class JgtdApplicationTests {
	@Autowired
	private DataMapper dataMapper;
	@Test
	void contextLoads() {
		List<Data> datas = dataMapper.queryAllData();
		for (Data data : datas) {
			log.debug(data.toString());
		}
	}

}
