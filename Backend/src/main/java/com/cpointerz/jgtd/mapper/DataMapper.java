package com.cpointerz.jgtd.mapper;

import com.cpointerz.jgtd.model.Data;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface DataMapper {
    @Select("select * from data")
    public List<Data> queryAllData();
    @Select("select * from data where id=#{id}")
    public Data queryDataById(Integer id);
    @Insert("insert into data(title, content, notTime, notTimes, beforeNot, notType) values(#{title}, #{content}, #{notTime}, #{notTimes}, #{beforeNot}, #{notType})")
    @Options(useGeneratedKeys = true, keyColumn = "id")
    public Integer addData(Data data);
    @Update("update data set content = #{content} where id=#{id}")
    public void updateDataContent(String content, Integer id);
}
