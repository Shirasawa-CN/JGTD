package com.cpointerz.jgtd.configuration;

import com.google.gson.Gson;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GlobalBeans {
    @Bean
    public Gson gson() {
        return new Gson();
    }
}
