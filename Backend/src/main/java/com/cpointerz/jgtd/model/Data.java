package com.cpointerz.jgtd.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Data {
    private Integer id;
    private String title;
    private String content;
    private String notTime;
    private Integer notTimes;
    private Boolean beforeNot;
    private String notType;
}
