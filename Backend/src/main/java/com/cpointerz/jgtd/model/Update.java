package com.cpointerz.jgtd.model;
/*是JGTD更新软件的类，用于获取更新文件和更新*/
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Update{
    /*
    check_update是启动更新的开关
    version是当前版本
    update_version是更新版本
    url是更新文件的url
    changlog是更新日志
    size是文件大小
    md5是更新文件的md5
    */
    public boolean check_update;
    public String version;
    public String update_version;
    public String url;
    public String changelog;
    public String size;
    public String md5;
}