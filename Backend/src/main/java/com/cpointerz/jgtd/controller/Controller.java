package com.cpointerz.jgtd.controller;

import com.cpointerz.jgtd.aspect.annotation.JsonReturn;
import com.cpointerz.jgtd.helper.FileHelper;
import com.cpointerz.jgtd.mapper.DataMapper;
import com.cpointerz.jgtd.model.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class Controller {
    @Autowired
    private DataMapper dataMapper;
    @Autowired
    private FileHelper fileHelper;

    @JsonReturn
    @GetMapping("/allData")
    public Object allData() {
        return dataMapper.queryAllData();
    }

    @JsonReturn
    @PostMapping("/createData")
    public Object createData(String title, String content, String notTime, Integer notTimes, Boolean beforeNot, String notType) {
        Data data = new Data();
        data.setTitle(title);
        data.setNotTime(notTime);
        data.setNotTimes(notTimes);
        data.setBeforeNot(beforeNot);
        data.setNotType(notType);
        data.setContent(""); // Content先设置为空，下面生成好markdown后再更新为md文件的路经
        Integer id = dataMapper.addData(data);
        log.info("ID: " + id);
        String mdLocation = fileHelper.saveContent(content, id);
        dataMapper.updateDataContent(mdLocation, id);
        return "Success";
    }

    @JsonReturn
    @PostMapping("/changeData")
    public Object changeData () {
        return null;
    }
}
