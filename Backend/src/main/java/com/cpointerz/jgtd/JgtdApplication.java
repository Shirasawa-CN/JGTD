package com.cpointerz.jgtd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@ComponentScan
@SpringBootApplication
public class JgtdApplication {

	public static void main(String[] args) {
		SpringApplication.run(JgtdApplication.class, args);
	}

}
